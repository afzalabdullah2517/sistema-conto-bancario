/* TerminaleBanca: file eseguibile direttamente da Java e che fornisce una console per l’operatore */
import java.time.LocalDate;

public class TerminaleBanca {
    public static void main(String[] args) {
        // Creare un conto corrente
        ContoCorrente cSiric = new ContoCorrente(0.0, true, 100.0); // Aggiunto un fido di 100.0

        System.out.println("Saldo iniziale: " + cSiric.getSaldo());

        // Bloccare il conto
        cSiric.Blocca();
        System.out.println("Saldo dopo aver bloccato il conto: " + cSiric.getSaldo());

        // Sbloccare il conto
        cSiric.Sblocca();
        System.out.println("Saldo dopo aver sbloccato il conto: " + cSiric.getSaldo());

        // Esempio di bonifico
        LocalDate dataRichiesta = LocalDate.parse("2023-10-09");
        LocalDate dataValuta = LocalDate.parse("2023-10-10");
        String descrizione = "Bonifico in uscita";
        String tipo = "Bonifico";
        double importo = 10.0;

        Movimento bonifico = new Movimento(dataRichiesta, dataValuta, descrizione, tipo, importo);
        cSiric.bonifico(bonifico);

        System.out.println("Saldo dopo il bonifico: " + cSiric.getSaldo());
    }
}
