import java.time.LocalDate;
public class Movimento{
	private LocalDate dataRichiesta;
	private LocalDate dataValuta;
	private String descrizione;
	private String tipologia;
	private double importo;
	
	public Movimento(LocalDate dRIniz, LocalDate dVIniz, String descrIniz, String tipoIniz, double importoIniz){
		this.dataRichiesta = dRIniz;
		this.dataValuta = dVIniz;
		this.descrizione = descrIniz;
		this.tipologia = tipoIniz;
		this.importo = importoIniz;
	}
	
	//GETTER
	public double getImporto(){
		return importo;
	}
	public String getTipologia(){
		return tipologia;
	}
	public String getDescrizione(){
		return descrizione;
	}
	public LocalDate getDataValuta(){
		return dataValuta;
	}
	public LocalDate getDataRichiesta(){
		return dataRichiesta;
	}
	
	//SETTER
	public void setImporto(double newImporto){
		importo = newImporto;
	}
	public void setTipologia(String newTipologia){
		tipologia = newTipologia;
	}
	public void setDescrizione(String newDescrizione){
		descrizione = newDescrizione;
	}
	public void setDataValuta(LocalDate newDataValuta){
		dataValuta = newDataValuta;
	}
	public void setDataRichiesta(LocalDate newDataRichiesta){
		dataRichiesta = newDataRichiesta;
	}
	// Metodo toString
    public String toString() {
        return "Movimento [tipologia=" + tipologia + ", importo=" + importo + "]";
    }
}


