import java.time.ArrayList;
import java.time.LocalDate;
public class ContoCorrente{
	private Persona intestatario;
	private double saldo;
	private boolean unblocked;
	private ArrayList<Movimento> ArrOperazioni; //cronologia operazioni
	private int i = 0;
	public ContoCorrente(double saldoIniz, boolean unblockedIniz){
			this.saldo = saldoIniz; //constructor
			this.unblocked = unblockedIniz;
			this.ArrOperazioni = new ArrayList<>();
	}
	
	//GETTER	
	public double getSaldo(){
		return unblocked?saldo:0;
	}

	//SETTER
	public void setSaldo(double newSaldo){
		if(unblocked){
			saldo = newSaldo;
		}
	}
	
	public void bonifico(Movimento operazione){
		ArrOperazioni.add(operazione);
		if(operazione.getImporto() > 0){
			saldo += operazione.getImporto();
		}
	}
	
	public void prelievo(Movimento operazione){
		ArrOperazioni.add(operazione);
		if(operazione.getImporto() > 0){
			saldo -= operazione.getImporto();
		}
	}
	
	public void Blocca(){
		unblocked = false;
	}
	public void Sblocca(){
		unblocked = true;
	}
	
	
}
